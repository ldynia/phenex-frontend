user nginx;
worker_processes auto;
pid /run/nginx.pid;

events {
  worker_connections 768;
}

http {
  ### Basic Configuration ###
  sendfile on;
  tcp_nopush on;
  tcp_nodelay on;
  keepalive_timeout 65;
  types_hash_max_size 2048;
  client_max_body_size 30M;

  include /etc/nginx/mime.types;
  default_type application/octet-stream;

  ### Log Format###
  log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_x_forwarded_for" "$http_user_agent"';

  ### Log ###
  error_log /dev/stderr warn;
  access_log /dev/stdout main;

  ### SSL Settings ###
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
  ssl_prefer_server_ciphers on;

  ### Gzip Settings ###
  gzip on;
  gzip_disable "msie6";
  gzip_vary on;
  gzip_proxied any;
  gzip_comp_level 6;
  gzip_buffers 16 8k;
  gzip_http_version 1.1;
  gzip_types
    text/js
    text/css
    text/xml
    text/plain
    text/javascript
    application/json
    application/javascript
    application/x-javascript
    application/xml
    application/xml+rss
    image/svg+xml;

  gzip_static on;

  ### Adding custom context headers
  add_header X-Forwarded-For $http_host;
  add_header X-Forwarded-Host $hostname;

  ### Virtual Host Configs ###
  include /etc/nginx/conf.d/*.conf;
}
